public class Subclass extends SuperClaSS 
{
public Subclass()
{
 System.out.println("Subclass(child1) - constructor");
}
public Subclass(int no)
{
 super(no); //new AmmaAppa(10); if the no arguments in super() ith will call the 0-arg cons in super class
 System.out.println("Subclass(child1) oneArugument - constructor");
}
public static void main(String[] args)
{
 //Subclass pp1 = new Subclass();
 Subclass pp = new Subclass(10);
 //implecit call
}
}