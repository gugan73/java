package java_interviewprgms;

import java.util.Scanner;

public class stringrev_palindrome {
	public static void main(String[] args) {
		stringrev_palindrome obj=new stringrev_palindrome();
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the String to be reversed : ");
		String s1=sc.nextLine();
		obj.stringbuffer(s1);
		String rev="";
		int len=s1.length();
		for (int i=len-1;i>=0;i--) {
			rev=rev+s1.charAt(i);
		}
		//for reverse string
		System.out.println(rev);
		
		// for palindrome 
		if(s1.equals(rev)) {
			System.out.println("palindrome");
		}
		else {
			System.out.println("Not a palindrome");
		}
	}

	private void stringbuffer(String s1) {
		StringBuffer sb=new StringBuffer(s1);
		System.out.println(sb.reverse());
		
	}

}
