public class Home
{
	//static refers to the Class Specification (information about the class)
	//ststic is a global variable The global variable must be out side the Main method
	static String Name= "Adithya Flats";
	static int age= 20;
	/*Varible Declared outside a Block or a Method is 
	Global Var but Non-Static*/
	/* Non Static refers to Information about the Instance (OBJECT)Specific information*/
	String Name1= "Gugan";// variable initialized
	int age1= 20; 
public static void main (String args[])
{
	// local variable must be declared inside the the Main method
	String Name2= "Gugan";//Varible Declared inside a Block or a Method is Local Var
	int age2= 20;
    System.out.println("Welcome to java world");
	System.out.println("The Name of Home is :"+Home.Name);
	System.out.println(Home.age);
	System.out.println(Name2);
	System.out.println(age2);
	// To create an (instance)obj 
	//Must be created inside the  method
	//memory referance of a class
	Home person=new Home();
	System.out.println(person.Name1);
	System.out.println(person.age1);
}
}