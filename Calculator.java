public class Calculator {
    public static void main(String args[]) {
        Calculator cc = new Calculator(); // creating a new object
        cc.divide(20); // passing a value to divide method
        cc.sub(10, 2); // passing two values to sub method
        System.out.println(cc.add()); // calling add() method without arguments
        System.out.println(cc.add(10)); // calling add() method with an argument
    }

    public int add() { // method 1 declaration
        return 30 + 40;
    }

    public int add(int a) { // method declaration if the return value is required
        return a + 73; // using the argument 'a' in the calculation
    }

    public void divide(int div) // Method 2 with arguments
    {
        System.out.println(div / 10);
    }

    public void sub(int div, int no) {
        System.out.println(div - no); // correcting the subtraction operation
    }
}
