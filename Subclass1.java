public class Subclass1 extends SuperClaSS 
{
public Subclass1()
{
 System.out.println("Subclass1(Child2) - constructor");
}
public Subclass1(int no)
{
 super(no); //new AmmaAppa(10); if the no arguments in super() ith will call the 0-arg cons in super class
 System.out.println("Subclass1(Child2) oneArugument - constructor");
}
public static void main(String[] args)
{
 //Subclass pp1 = new Subclass();
 Subclass1 pp = new Subclass1(10);
 //implecit call
}
}