//DUPLICATE ITTEMS IN AN ARRAY (TIME COMPLEXITY O(N*N))
package java_interviewprgms;

import java.util.Scanner;

public class Duplicate_elemarray {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the length of arry : ");
		int n = sc.nextInt();
		boolean noduplicate=true;
		String[] str = new String[n];
		System.out.println("Enter the elements for array: ");
		for (int i = 0; i < n; i++) {
			str[i] = sc.next();
		}
//		for(int j=0;j<str.length;j++) {
//			System.out.println(str[j]);
//		}

		// duplicate check
		for (int d = 0; d < str.length; d++) {
			for (int e = d + 1; e < str.length; e++) {
				if (str[d].equals(str[e])) {
					System.out.println("The duplicate element is :" + str[d]);
					noduplicate=false;
				}
			}
		}
		if(noduplicate) {
			System.out.println("No duplicate items");
		}
	}

}
