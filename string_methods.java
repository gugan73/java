package learnstring;

public class string_methods {
	public static void main(String[] args) {
		String name="gugan";
		String name1="Gugan";
		System.out.println(name.charAt(0));// Display the character in the index position
		System.out.println(name.compareTo(name1));//compare the the given strings based on case and if it is equal return 0 or some value based on ASCII VALU
		System.out.println(name.compareToIgnoreCase(name1));// Same as the Compared to method but ignore the case
		System.out.println(name.concat(name1));// the two string string concatenation should use this method
		System.out.println(name.contains("gan"));// check the give set of character is in the string
		System.out.println(name.equals(name1));// this is used to check if the value is equal  instead of == (== in string compare the memory location is equal or not )
		System.out.println(name.equalsIgnoreCase(name1));// ignore the case compare
		System.out.println(name.hashCode());// returns the memory location of the given string 
		System.out.println(name.startsWith("m"));// check if the String starts with the given char
		System.out.println(name.endsWith("n"));
		System.out.println(name.isEmpty());
		System.out.println(name.indexOf("g"));// return the index of the given character. check from from first 
		System.out.println(name.lastIndexOf("g"));//return the index of the given character. check from from last
	}
}
