public class ConstructorOverloading
{
	// java will always have a default constructor invisible
	public ConstructorOverloading(int no)
	{
	System.out.println("constructor");
	}
	//zero arg constructor
	public ConstructorOverloading()
	{
	System.out.println("overloading");
	}
	public static void main (String arg[])
	{
		ConstructorOverloading data1=new ConstructorOverloading(10);
		ConstructorOverloading data2=new ConstructorOverloading();
	}
}
