package java_interviewprgms;

public class findindexof_keyinarr {
    public static void main(String[] args) {
        
        
        
        findindexof_keyinarr ar=new findindexof_keyinarr();
        ar.findIndex([23,29,30,21,16,10,29,27,19,12,30,20,10,27,30,24,20,27,22,16,27,24,24,11,12,29],26,29);
       public int[] findIndex(int a[], int N, int key) 
        {
           int count = 0;
            
            // Count occurrences of key
            for (int i = 0; i < N; i++) {
                if (a[i] == key) {
                    count++;
                }
            }
            
            // Create an array to store indices
            int[] arr = new int[count];
            int index = 0;
            
            // Store indices where key is found
            for (int i = 0; i < N; i++) {
                if (a[i] == key) {
                    arr[index] = i;
                    index++;
                }
            }
            
            return arr;
            
        }
    }
}
