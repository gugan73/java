//TYPES OF CONSTRUCTOR 
public class BookShop
{
//Default constructor 
public BookShop(int no)
{
System.out.println("Hi i am 1-arg constructor");
}
//Zero arg constructor
public  BookShop()
{
System.out.println("Hi i am 0-arg constructor"); 
}
public  BookShop(String str)
{
System.out.println("Hi i am 1-arg constructor"); 
} 
public static void main(String[] args)
{
 BookShop book2 = new BookShop(100);
 BookShop book = new BookShop();
 BookShop book3 = new BookShop("abc");
}
}