package java_interviewprgms;

public class Secondlargest_array {
    public static void main(String[] args) {
        int arr[]= {10,10,10};
        //int[] arr = {61 ,30 ,28 ,47, 42 ,25 ,41, 56, 27 ,45 ,44 ,34 ,46 ,35 ,58 ,36 ,60 ,29 ,53 ,55 ,32 ,31 ,33 ,59 ,50 ,51 ,52 ,37 ,39 ,38 ,43 ,49 ,54 ,57 ,40 ,26 ,48};
        int max = arr[0];
        int slargest = -1; // Initialize slargest with the minimum possible integer value

        for(int i = 0; i < arr.length; i++) {
            if(arr[i] > max) {
                slargest = max;
                max = arr[i];
            } else if(arr[i] > slargest && arr[i] != max) {
                slargest = arr[i];
            }
        }

        System.out.println(slargest);
    }
}
