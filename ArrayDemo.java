package LearnArray;

public class ArrayDemo {
	int[] marks = { 85, 86, 47, 89, 93 };
	public static void main(String[] args) {
		ArrayDemo ad = new ArrayDemo();
		ad.evenusingwhile();
		ad.oddusingwhile();
		ad.reverse();
		ad.total();
	}

	private void evenusingwhile() {
		int i = 0;
		int[] marks = { 85, 86, 47, 89, 93 };
		while (i < 5) {
			if (marks[i] % 2 == 0)
				System.out.println(marks[i]);
			i++;
		}
		System.out.println("-------------------------------------------");
	}

	private void oddusingwhile() {
		int i = 0;
		int[] marks = { 85, 86, 47, 89, 93 };
		while (i < 5) {
			if (marks[i] % 2 != 0)
				System.out.println(marks[i]);
			i++;
		}
		System.out.println("-------------------------------------------");
	}

	// reverse
	private void reverse() {
		int i = 4;
		int[] marks = { 85, 86, 47, 89, 93 };
		while (i >= 0) {
			System.out.println(marks[i]);
			i--;
		}
		System.out.println("-------------------------------------------");
	}

	// total
	private void total() {
		int i = 0;
		int total = 0;
		while (i <= 4) {
			total = total + marks[i];
			i++;
		}
		System.out.println(total);
		System.out.println("-------------------------------------------");
	}

}
