package com.IndianBank;
import com.indianBank.deposit.InterestCalculation;// importing the package fropm previoyyus program
public class Savings
{
public static void main (String arg[])
{
	System.out.println ("Calling a Method from the Other  Packages");
	InterestCalculation ic= new InterestCalculation();
	ic.Calculate(7.5f);/* here the method calculate is called from the package 
com.indianBank.deposit.InterestCalculation in the program InterestCalculation*/
}
}
// to Compile a java program with packages use :
//  	javac -d . Classname.java
// After class file created .java file  to run the file use :
//   	java Package name.Class name