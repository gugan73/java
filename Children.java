// Abstraction child class and Dynamic binding.
public class Children extends parents
{
public static void main(String arg[])
{
Children c=new Children();// Static binding or Early binding
parent c=new Children();// can create a object for parent in the memory of children using dynamic binding or late binding.// it cann access only the overriden method of parent and its own method from its class .
c.study();
c.play();
c.motivate();
c.workhard();
}
public void play()
{
System.out.println("playing");
}
public void study()
{
System.out.println("Law");
}
}
