package learnstring;
public class Learn_String {
	String name;
	int id;
// constructor
	public Learn_String(String name,int id) {
		this.name = name;
		this.id=id;
	}
	// method
	@Override// automatically called when an object is printed
    //  here the integer cannot be printed directly . so  whitespace is used before printing or returning the value in integer .
	public String toString() {
		//return this.name;
		return " "+this.id;
	}
	public static void main(String[] args) {
		// String s1=new String("obj using new keyword");
		// whenever we print a object tostring() method will be called automatically
		Learn_String ls = new Learn_String("tostring method",420);
		String s1=new String("Gugan");
		System.out.println(s1); 
		System.out.println(ls);// Simply prints the learnstring.Learn_String@d041cf in the output without overriding tostring() method
	}	
}
