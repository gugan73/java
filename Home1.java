public class Home1
{
	//static refers to the Class Specification (information about the class)
	//ststic is a global variable The global variable must be out side the Main method
	static String Name="G3";// variable not initialized
	static int age=20;
	/*Varible Declared outside a Block or a Method is 
	Global Var but Non-Static*/
	/* Non Static refers to Information about the Instance (OBJECT)Specific information*/
	String Name1;// variable not initialized
	int age1;
	float weight1;
	char lastName1;
public static void main (String args[])
{
	// local variable must be declared inside the the Main method
	String Name2="Gugan";//Varible Declared and initialized
	int age2=20;
System.out.println("Welcome to java world");
	System.out.println("The Name of Home is :"+Home1.Name);
	System.out.println("The age of Home is :"+Home1.age);
	System.out.println(Name2);
	System.out.println(age2);
	// To create an (instance)obj 
	//If the variable is not initialiized it shows the default value omly in global var
	//multiple class can be created
	Home1 person=new Home1();
	System.out.println("The Name of the person1 is :"+person.Name1);
	System.out.println("The age of the Person1 is :"+person.age1);
	System.out.println("The Weight of the person1 is :"+person.weight1);
	System.out.println("The Last Name of the Person1 is :"+person.lastName1);
}
}