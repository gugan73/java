package LearnArray;

public class MultiDimensionArray {
	public static void main(String[] args) {
		// 3D array
		int[][][] threeDArrayMark = {
				// Class 1
				{ { 71, 92, 83 }, { 74, 85, 66 }, { 75, 89, 92 } },
				// Class 2
				{ { 100, 61, 62 }, { 83, 64, 55 }, { 86, 77, 58 } },
				// Class 3
				{ { 99, 60, 94 }, { 72, 83, 74 }, { 95, 86, 67 } } };

		System.out.println("Element of Class 1, row 1, column 2: " + threeDArrayMark[0][1][2]);
		System.out.println("Element at Class 2, row 2, column 0: " + threeDArrayMark[1][2][0]);
		System.out.println("Element at Class 3, row 1, column 2: " + threeDArrayMark[2][1][2]);
	}
}
