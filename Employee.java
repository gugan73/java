//PROGRAM FOR INTERFACE
public class Employee implements Rules
{
public static void main(String[] args)
{
Rules manager=new Employee();//dynamic binding concept is also applicable in interface without any changes
//ee.comeontime();
//ee.takeleave();
//ee.getsalary();
//System.out.println(ee.leave);//becaue the variable in interface are final and static calling using object
System.out.println(Employee.leave);//calling using class name 
System.out.println(Rules.leave);//calling using interface name
manager.comeontime();
manager.takeleave();
}
public void comeontime()
{
System.out.println("TIME : 9 AM");
}
public void takeleave()
{
System.out.println("LEAVE : 12 DAYS");
}
public void getsalary()
{
System.out.println("RS : 25,0000");
}
}